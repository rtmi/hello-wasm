package main

import (
	"errors"
	"fmt"
	"strconv"
	"syscall/js"
)

var done = make(chan struct{})

func main() {
	callback := js.NewCallback(printFib)
	defer callback.Release()
	setPrintFib := js.Global().Get("setPrintFib")
	setPrintFib.Invoke(callback)
	<-done
}

func printFib(args []js.Value) {
	n, err := extractArg(args)
	if err != nil {
		fmt.Println("Fibonacci requires a number arg")
	} else {
		t := fib(n)
		fmt.Println(t)
	}
	done <- struct{}{} // Notify printFib has been called
}

func fib(n int) int {
	var t int
	a := 0
	b := 1
	for i := 0; i < n; i++ {
		t = a + b
		a = b
		b = t
	}
	return t
}

func extractArg(args []js.Value) (int, error) {
	if len(args) == 0 {
		return 0, errors.New("Arg required")
	}
	a := args[0].String()
	n, err := strconv.Atoi(a)
	if err != nil {
		return 0, errors.New("Arg required")
	}
	return n, nil
}
