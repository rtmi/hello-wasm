# Hello WASM

Experiment to learn WASM
Follows the article by [Nicolas Lepage](https://medium.zenika.com/go-1-11-webassembly-for-the-gophers-ae4bb8b1ee03)
 adjusted for using the WASM target in Go 1.11


## Quickstart

1. git clone https://github.com/patterns/hello-wasm
2. docker build --network=host -t hello hello-wasm
3. docker run -ti --rm --network=host hello
4. Visit localhost:8080, click the wasm_exec.html page, enable dev tools to observe the console output.
5. To obtain the compiled WASM file, mount the local subdir to copy the file:

```
$ docker run -ti --rm -v $PWD:/app --entrypoint ash hello
# cp /usr/local/wasm/test.wasm /app
# exit
$ ls -l ./test.wasm

```


## Credits

Hajime Hoshi's
 [Ebiten](https://dev.to/hajimehoshi/gopherjs-vs-webassembly-for-go-148m)

Nicolas Lepage's
 [Intro](https://medium.zenika.com/go-1-11-webassembly-for-the-gophers-ae4bb8b1ee03)

[Emscripting a C library to Wasm](https://developers.google.com/web/updates/2018/03/emscripting-a-c-library)

Go's WASM
 [Start Files](https://github.com/golang/go/tree/master/misc/wasm)
