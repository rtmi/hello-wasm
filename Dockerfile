# build stage
FROM golang:rc-alpine AS build-env
ENV GOOS=js \
    GOARCH=wasm 

COPY . /go/src/app/

RUN cd /go/src/app \
    && go build -o test.wasm main.go


# final stage
FROM node:alpine
COPY --from=build-env /go/src/app/* /usr/local/wasm/
RUN npm install http-server -g
WORKDIR /usr/local/wasm
ENTRYPOINT ["http-server"]

